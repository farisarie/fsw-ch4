"use strict";

class Game {
  constructor() {
    this.gameFinished = false;
  }

  playGame(playerChoose) {
    if (!this.gameFinished) {
      const computerRockButton = document.getElementsByClassName(
        "computer-move-rock"
      );
      const computerPapersButton = document.getElementsByClassName(
        "computer-move-paper"
      );
      const computerScissorsButton = document.getElementsByClassName(
        "computer-move-scissors"
      );

      const moves = ["rock", "paper", "scissors"];
      const computerMove = moves[Math.floor(Math.random() * moves.length)];
      console.log(playerChoose + "tesya");
      console.log(computerMove + "tesya");
      const resultMessage = this.getResultMessage(playerChoose, computerMove);
      this.updateResultElement(resultMessage);

      this.highlightComputerMove(
        computerMove,
        computerRockButton,
        computerPapersButton,
        computerScissorsButton
      );

      this.gameFinished = true;
      this.disablePlayerButton();
    }
  }

  getResultMessage(playerChoose, computerMove) {
    if (playerChoose === computerMove) {
      return "DRAW!";
    } else if (
     
      (playerChoose === "rock" && computerMove === "scissors") ||
      (playerChoose === "paper" && computerMove === "rock") ||
      (playerChoose === "scissors" && computerMove === "paper")
    ) {
      return "Player 1 win!";
    } else {
      return "COM win!";
    }
  }

  updateResultElement(message) {
    let resultElement = document.getElementById("result");
    resultElement.textContent = message;
    resultElement.style.padding = "2.6rem";
    resultElement.style.backgroundColor = "#035b0c";
    resultElement.style.borderRadius = "10px";
    resultElement.style.fontSize = "6.2rem";
    resultElement.style.color = "#Ffffff";
    resultElement.style.transform = "rotate(-25deg)";
  }

  highlightComputerMove(
    computerMove,
    computerRockButton,
    computerPapersButton,
    computerScissorsButton
  ) {
    switch (computerMove) {
      case "rock":
        this.highlightButton(computerRockButton);
        break;
      case "paper":
        this.highlightButton(computerPapersButton);
        break;
      case "scissors":
        this.highlightButton(computerScissorsButton);
        break;
      default:
        break;
    }
  }

  highlightButton(buttons) {
    buttons[0].style.backgroundColor = "#c4c4c4";
    buttons[0].style.padding = "2.6rem";
    buttons[0].style.borderRadius = "15%";
  }

  disablePlayerButton() {
    const playerButtons = document.querySelectorAll(".btn");
    playerButtons.forEach((button) => {
      button.disabled = true;
    });
  }

  enablePlayerButton() {
    const playerButtons = document.querySelectorAll(".btn");
    playerButtons.forEach((button) => {
      button.disabled = false;
    });
  }

  restartDesign() {
    let computerRockButton = document.getElementsByClassName(
      "computer-move-rock"
    );
    let computerPapersButton = document.getElementsByClassName(
      "computer-move-paper"
    );
    let computerScissorsButton = document.getElementsByClassName(
      "computer-move-scissors"
    );

    computerRockButton[0].style.backgroundColor = "transparent";
    computerRockButton[0].style.padding = "2.6rem";
    computerRockButton[0].style.border = "none";

    computerPapersButton[0].style.background = "transparent";
    computerPapersButton[0].style.padding = "2.6rem";
    computerPapersButton[0].style.border = "none";

    computerScissorsButton[0].style.background = "transparent";
    computerScissorsButton[0].style.padding = "2.6rem";
    computerScissorsButton[0].style.border = "none";

    let resultElement = document.getElementById("result");
    resultElement.textContent = "VS";
    resultElement.style.padding = "";
    resultElement.style.backgroundColor = "";
    resultElement.style.borderRadius = "";
    resultElement.style.color = "#bd0000";
    resultElement.style.transform = "";
    resultElement.style.fontSize = "12.4rem";
  }
}

class GameApp {
  constructor() {
    this.game = new Game();
    this.initEventListeners();
  }

  initEventListeners() {
    const playerButtons = document.querySelectorAll(".btn");
    playerButtons.forEach((button) => {
      button.addEventListener("click", () => {
        let playerChoose = button.dataset.move;
        this.game.playGame(playerChoose);
      });
    });

    const restartButton = document.querySelector(".btn-restart");
    restartButton.addEventListener("click", () => {
      this.restartGame();
    });
  }

  restartGame() {
    this.game.gameFinished = false;
    this.game.restartDesign();
    this.game.enablePlayerButton();
  }
}

const gameApp = new GameApp();
